#!/bin/python
from __future__ import print_function
import sys
import subprocess as sub
import numpy as np
import os.path as path
import requests
import traceback

################ Exceptions ################

class ExitCodeException(Exception):
  """ Base class for all exceptions which set the exit code"""
  def getExitCode(self):
    "meant to be overridden in subclass"
    return 1

def handleUncaughtException(exctype, value, trace):
  oldHook(exctype, value, trace)
  if isinstance(value, ExitCodeException):
    sys.exit(value.getExitCode())

sys.excepthook, oldHook = handleUncaughtException, sys.excepthook

class RunException(ExitCodeException):
    def __init__(self, returncode, args, err, out):
        self.returncode = returncode
        self.args = args
        self.err = err
        self.out = out
        super(RunException, self).__init__(returncode, args, err, out)

    def getExitCode(self):
        return 3

class DiffException(ExitCodeException):
    def __init__(self, msg, outfile):
        self.msg = msg
        self.outfile = outfile

    def getExitCode(self):
        return 5

################ Functions ################
def runkat(katfile):

    try:
        p = sub.Popen(["kat","--test",katfile], stdout=sub.PIPE, stderr=sub.PIPE)
        out, err = p.communicate()
    finally:
        if 'p' in locals():
            try:
                p.kill()
            except OSError:
                pass

    encoding=sys.stdout.encoding
    if encoding==None:
        encoding='utf-8'
    out = out.decode(encoding)
    err = err.decode(encoding)
    
    print(str(out))
    print(str(err), file=sys.stderr)

    if p.returncode != 0:
        raise RunException(p.returncode, "kat "+katfile, err, out)

    return [out,err,p.returncode]

def diff(katfile):
    diffFound = False

    katdir=path.dirname(katfile)
    katname=str(path.basename(katfile)).split('.')[0]

    REF_DIR=path.join(katdir,'reference')
     
    if not path.exists(REF_DIR):
        raise Exception("Suite reference directory doesn't exist: " + REF_DIR)

    ref_file = path.join(REF_DIR,katname+'.out')
    out_file = path.join(katdir,katname+'.out')
    tol_file = path.join(REF_DIR,katname+'.tol')

    if not path.exists(ref_file):
        raise Exception("Reference file doesn't exist for " + ref_file, ref_file)

    if not path.exists(out_file):
        raise Exception("Cannot locate output file for " + out_file, out_file)

    if path.exists(tol_file):
        print("Tolerance file found, setting tolerance")
        diff_rel_eps = np.loadtxt(tol_file, dtype=np.float64)
        if diff_rel_eps.size != 1:
            raise DiffException("Tolerance file sized incorrecly: " + tol_file, tol_file)
        else:
            print("Tolerance = "+str(diff_rel_eps))
    else:
        diff_rel_eps = np.array(1e-12)

    ref_arr = np.genfromtxt(ref_file, dtype=np.float64, comments='%')
    out_arr = np.genfromtxt(out_file, dtype=np.float64, comments='%')

    if ref_arr.shape != out_arr.shape:
        raise DiffException("Reference and output are different shapes", out)

    # for computing relative errors we need to make sure we
    # have no zeros in the data
    nzix = (ref_arr != 0)

    rel_diff = np.abs(out_arr-ref_arr)
    # only compute rel diff for non zero values
    rel_diff[nzix] = np.divide(rel_diff[nzix], np.abs(ref_arr[nzix]))

    diff = np.any(rel_diff >= diff_rel_eps)

    if diff:
        diffFound = True

        print("Difference larger than " + str(diff_rel_eps))
        # store the rows which are different
        ix = np.where(rel_diff >= diff_rel_eps)[0][0]

        print('Refernce Array: '+str(ref_arr[ix]))
        print('Output Array: '+str(out_arr[ix]))
        print('Max relative difference: '+str(np.max(rel_diff)))
        raise DiffException("Difference larger than " + str(diff_rel_eps), katfile)

    else:
        mx = np.max(rel_diff) 
        print("All differences smaller than " + str(diff_rel_eps))
        print('Max relative difference: '+str(mx))

    return


if __name__ == "__main__":
    print("Kat Test Started, Arguments: "+str(sys.argv))
    test_kat = sys.argv[1]
    runkat(test_kat)
    try:
        diff(test_kat)
    finally:
        if len(sys.argv) > 2:
            try:
                katdir=path.dirname(test_kat)
                katname=str(path.basename(test_kat)).split('.')[0]
                out_file = path.join(katdir,katname+'.out')
                print('Uploading: '+str(path.basename(out_file))+" To: "+str(sys.argv[2]))
                with open(out_file, 'rb') as f: r = requests.post(sys.argv[2], files={'file': f},verify=False)
                print("Server Returns: " + str(r.text))
                r.raise_for_status()
            except Exception:
                print(traceback.format_exc())
