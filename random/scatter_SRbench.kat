#-----------------------------------------------------------------------
#	GEO file for the time from May 05
# 
#     M. Malec, 2005-05-31
#
#
# Differences from geo_2005_05_ii.kat (alias geo_2005_05_v001.kat): 
#
# * new losses of MI end mirrors (650 ppm)
# * new thermal lense of 8.5 km corresponding to 1170 W intracavity power
# * new SRC length (ssouth = 1.109 m)
# * new Schnupp asymmetry of 61.7 mm (and fineadjusted ssouth and swest)
# * new MSR parameters (R = 0.9814)
#
# Differences from geo_2005_04.kat: 
# 
# * Modulation indizes for MI and SR determined by measurement on 2005-02-10
#   (labbook-p. 2054), for PR by measurement on 2004-11-18 (labbook-p. 1797)
# * new MPR with T = 900 ppm  (see labbook-p. 2264)
# * new PRC length after MPR installation: longer by 1 mm (see
#   labbook-p. 2349, with resonant MI frequency = 14.904875 MHz instead of
#   14.904889 MHz) 
# * new MI frequency: 14.904875 MHz
# * optional fiber after MPR (as estimated by Ken)
#
# Differences from geo_2005_05.kat: 
#
# * new MPR with 900 ppm transmission and 50 ppm loss => R = 0.999050
# * new MI arm mirror transmission of 150 ppm instead of 50 ppm
# * new BSAR of R = 150 ppm instead of 50 ppm (and 30 ppm loss)
# * new thermal lense of 6.1 km for intracavity power of 1.64 kW 
# * new MSR with R = 0.981 and T = 0.01895
#
#

#debug 320


l i1 1.7 0 nMU3in

#gauss beam_in i1 nMU3in 268u -550m 

mod  eom3 $fPR 0.126433 2 pm 0 nMU3in nMU3_1_1    # PR control
s s0 0 nMU3_1_1 nMU3_1_2
mod  eom4 $fSR 0.199417 2 pm 0 nMU3_1_2 nMU3_2_1     # Schnupp1 (SR control)
s s1 0 nMU3_2_1 nMU3_2_2
mod  eom5 $fMI 0.1225 2 pm 0 nMU3_2_2 nMU3_3_1     # Schnupp2 (MI control)
s s2 0 nMU3_3_1 nMU3_3_2
lens lpr 1.8 nMU3_3_2 nMU3_4_1
s s3 0 nMU3_4_1 nMU3_4_2
# some rather arbitrary thermal lense for the isolators and the EOMs:
lens therm 5.2 nMU3_4_2 nMU3_5_1
s s4 0 nMU3_5_1 nMU3_5_2
isol d2 120 nMU3_5_2 nMU3out               	# Faraday Isolator

# 070502 corrected length with respect to OptoCad (Roland Schilling)
s    smcpr3 4.391 nMU3out nBDIPR1  
bs*  BDIPR 50 30 0 45 nBDIPR1 nBDIPR2 dump dump

s    smcpr4 0.11 nBDIPR2 nMPRo


##------------------------------------------------------------ 
## main interferometer ##
##
## Mirror specifications for the _final_ optics are used.
#
## first (curved) surface of MPR
m    mPRo 0 1 0 nMPRo nMPRi
#attr mPRo Rc -1.867     # Rc as specified 
attr mPRo Rc -1.85842517051051 # Rc as used in OptoCad (layout_1.41.ocd)
s    smpr 0.075 1.44963 nMPRi nPRo
# second (inner) surface of MPR
#m*   MPR 1000 38 0. nPRo nPRi   	# 1000ppm power recycling
#m   MPR 0 1 0. nPRo nPRi       	# no power recycling
#m   MPR 0.9865 0.0135 0. nPRo nPRi       	# T=1.35% PR
m   MPR 0.99905 0.0009 0. nPRo nPRi       	# T= 900 ppm PR
#attr MPR ybeta 2u
#s    swest 1.145 nPRi nBSwest		# old length with T_PR=1.35%
s    swest 1.1463 nPRi nBSwest		# new length with T_PR=900 ppm

#   next two lines replace last one to include fibre attenuator - need also
#   90degrees on MPR 
#s    swest 1.146 nPRi nBSwesta
#bs*  FIBRE 995000 0 0 0 nBSwesta dump nBSwest dump 


## BS
##
##                              
##                       nBSnorth     ,'-.
##                             |     +    `. 
##                             |   ,'       :'
##            nBSwest          |  +i1      +
##         ---------------->    ,:._  i2 ,'
##                             + \  `-. +       nBSeast
##                           ,' i3\   ,' ---------------
##                          +      \ +
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nBSsouth
##                             -   |
##                                 |
##                                 |


bs   BS 0.4859975 0.5139975 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#bs   BS 0.5139975 0.4859975  0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#bs   BS 0.50 0.50 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
#attr BS ybeta 2u
s    sBS1a 0.041 1.44963 nBSi1 nBSi1b
# here thermal lense of beam splitter (Roland: f about 1000m for 10kW at BS)
lens bst 8.5k nBSi1b nBSi1c
s    sBS1 0.051 1.44963 nBSi1c nBSi2
s    sBS2 0.091 1.44963 nBSi3 nBSi4
bs   BS2 150u 0.99982 0 -27.9694 nBSi2 dump nBSeast nBSAR  
bs   BS3 150u 0.99982 0 -27.9694 nBSi4 dump nBSsouth dump	

## north arm
s snorth1 598.5682 nBSnorth nMFN1
bs* MFN 150 10 0.0 0.0 nMFN1 nMFN2 dump dump
attr MFN Rc 666

s snorth2 597.0241 nMFN2 nMCN1
m* MCN 640 10 -0.0 nMCN1 dump
attr MCN Rc 636


## east arm
s seast1 598.4497 nBSeast nMFE1
bs* MFE 150 10 0.0 0.0 nMFE1 nMFE2 dump dump
attr MFE Rcx 665	# 71 W
attr MFE Rcy 662	# 71 W
#attr MFE Rc 663.75	# perfect curvature
#attr MFE Rcx 666.41	# 66W
#attr MFE Rcy 663.75	# 66W
#attr MFE Rcy 660.75	# 75W
#attr MFE Rcx 663.75	# 75W

s seast2 597.0630 nMFE2 nMCE1
m* MCE 640 10 0.0 nMCE1 dump  
attr MCE Rc 622



## south arm
s ssouth 1.109 nBSsouth nMSRi

m MSR 0.9814 0.01855 1.419 nMSRi nMSRo 	# SRSB 9.016315 MHz
#m MSR 0 1 0 nMSRi nMSRo 	# no SR

/*
##-- output optics: ----------------------                   
                                                             
s sout1 1.8 nMSRo nBDO1i                                     
bs* BDO1 50 0.0 0.0 5.0 nBDO1i nBDO1o dump dump              
attr BDO1 Rc 6.72                                            
                                                             
s sout2 4.855 nBDO1o nLO1i                                   
                                                             
lens LO1 0.5 nLO1i nLO1o # first lens after TCOc.            
                                                             
s sout3 0.703 nLO1o nLO2i # computed telescope length        
                                                             
lens LO2 -0.03 nLO2i nLO2o                                   
                                                             
s sout4 1.0 nLO2o nout  # length with telescope              
*/

##------------------------------------------------------------ 
## commands
maxtem 2
time
#trace 8
phase 3
# MC1 cavity
#cav mc1 MMC1a nMC1_0 MMC1a nMC1_5
# MC2 cavity
#cav mc2 MMC2a nMC2_0 MMC2a nMC2_5
# PR cavity (north arm)
cav prc1 MPR nPRi MCN nMCN1 
# PR cavity (east arm)
cav prc2 MPR nPRi MCE nMCE1 
# SR cavity (north arm)
cav src1 MSR nMSRi MCN nMCN1 
# SR cavity (east arm)
#cav src2 MSR nMSRi MCE nMCE1 

##------------------------------------------------------------ 

#pause

const fSR  9012589
const fMI  14.904927M	#14.904875M
const fPR  37.16M	
const phip -50
const phiq 40

fsig sig1 BDIPR 1 0

pd2 TFP $fMI $phip 1 nMSRo
pd2 TFQ $fMI $phiq 1 nMSRo
xaxis sig1 f lin 100 6000 590
func y = $x1
put TFP f2 $y
put TFQ f2 $y


yaxis log abs:deg

gnuterm no

