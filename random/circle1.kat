#-----------------------------------------------------------------------
# circle1.kat           Input File for FINESSE (www.rzg.mpg.de/~adf)
#
#
# 29.04.2004 by  Andreas Freise (andreas.freise@ego-gw.it)
#------------------------------------------------------------ 

#const fPDH 6.284080M              # modultion for PDH signals
const fMI 6.264080M              # modultion frequency currently used
#const fMI 6.264580M              # modultion frequency optimised for simulation

##################### LASER #####################

# input laser, here waist of the input modecleaner (IMC)
l i1 6 0 0 nin
#gauss g1 i1 nin 4.6e-3 -42.836m # tuned to measured beam at NE
#gauss g1 i1 nin 5e-3 -42.836m # nomial value


# output mirror of IMC
bs IMC2i 0 1 0 -45.0 nin dump nIMC2i1 dump
s  sIMC2 0.034 1.44963 nIMC2i1 nIMC2I2
bs IMC2o 0 1 0 -29.20 nIMC2I2 dump nIMC2o1 dump

# modulator for simplicity after IMC
s s0 1m nIMC2o1 nEO1
mod eom1 $fMI .1 1 pm 0 nEO1 nEO2
#mod eom2 $fPDH .1 1 pm 0 nEO2a nEO2

# distance from OptoCad 
s sM4 0.151 1 nEO2 nM4a

# gauss g2 derived from g1, just to force mode mismatch 
# to be computed on M4
#gauss g2 sM41 nM4i 5.0000014m 132.7421m 5.000009m 139.85894m

bs M4AR1 0 1 0 -46.71 nM4a dump nM4i1 dump
s sM41 12m 1.44963 nM4i1 nM4i2
bs M4HR 0.999 0.001 0 -30.15 nM4i2 nM4i3 dump dump 
s sM42 12m 1.44963 nM4i3 nM4i4
bs M4AR2 0 1 0 30.14 nM4i4 dump nM4b dump 

# mode matching telescope on IB
s s1 0.286 nM4b nM51 # distance from OpotoCad

# angle of incidence on M5: OptoCad -3.3 deg, Magali -4 deg 
bs M5 1 0 0 -3.3 nM51 nM52 dump dump
attr M5 Rc -1.22
s s2 0.545 nM52 nM61
# angle of incidence on M6: OptoCad  5.0 deg, Magali 4.9 deg 
bs M6 1 0 0 5 nM61 nM62 dump dump
attr M6 Rc 3.16

s s3 .596 nM62 nD1

m Dia 0 1 0 nD1 nD2

s s3b 4.68 nD2 nMPR1 # distance M6-MPR is is 5.276

######################## CENTER #####################

# power recycling mirror
m MPRo 0 1 0 nMPR1 nMPR2 # secondary surface, AR coated, curved
attr MPRo Rc -4.27 # tuned to fit beam measurement at NE
#attr MPRo Rc -4.3 # nominal value Rc=4.3m
s sP 0.03 1.44963 nMPR2 nMPR3
m MPRi 0 0.0783 0 nMPR3 nMPR4

#s sS 6.11325 nMPR4 nBSs # ?
s sS 5.96896 nMPR4 nBSs # from OptoCad

# Beamsplitter
bs BS 0.5025 0.49745 0 -44.978 nBSs nBSw nBSi1 nBSi3       
s sBS1 0.0632 1.44963 nBSi1 nBSi2
s sBS2 0.0632 1.44963 nBSi3 nBSi4
bs BSAR1 0.00052 0.99943 0 -29.125 nBSi2 dump nBSn nBSAR       
bs BSAR2 0 1 0 -29.125 nBSi4 dump nBSe dump      

######################## NORTH ARM #####################

#s sNs 6.3807 nBSn nMNI1  # as in Siesta card
#s sNs 6.32009 nBSn nMNI1 # as in Siesta card minus optical path of BS
s sNs 6.19618 nBSn nMNI1  # as in OptoCad

#gauss gNI MNIAR nMNI1 17m -700 # force mode-mismatch here

# North Input Mirror T=11.8%, Loss=1.8%
# (one could also use e.g. T=13.6%, Loss=50ppm)
m MNIAR 0 1 0 nMNI1 nMNIi1     # secondary surface (AR coated)
s sMNI .097 1.44963 nMNIi1 nMNIi2
m MNI 0.864 0.118 0 nMNIi2 nMNI2      
#attr MNI ybeta 1u              # some arbritrary mis-alignment

s sNl 2999.9 nMNI2 nMNE1       # cavity length

# North End Mirror T=50ppm, Loss=50ppm
#m MNE 0.9999 50u 0 nMNE1 nMNE2   
m MNE 0.9999 50u 0.59814 nMNE1 nMNE2   
attr MNE Rc 3530               # radius of curvature of NE
attr MNE ybeta -1u             # some arbritrary mis-alignment
# -1urad ty requires phi=0.59814 

######################## WEST ARM #####################

#s sWs 5.5328 nBSw nMWI1  # as in Siesta card
s sWs 5.254 nBSw nMWI1  # as in OptoCad


#gauss gWI MWIAR nMWI1 17m -700 # force mode-mismatch here

# West Input Mirror T=11.66%, 
m MWIAR 0 1 0 nMWI1 nMWIi1     # secondary surface (AR coated)
s sMWI .097 1.44963 nMWIi1 nMWIi2
m MWI 0.883 0.1166 0 nMWIi2 nMWI2      
#attr MWI ybeta 1u              # some arbritrary mis-alignment

s sWl 2999.9 nMWI2 nMWE1       # cavity length

# West End Mirror T=50ppm, Loss=50ppm
#m MWE 0.9999 50u 0.610935 nMWE1 nMWE2   
m MWE 0.9999 50u 0.0 nMWE1 nMWE2   
attr MWE Rc 3620               # radius of curvature of WE
#attr MWE ybeta -1u             # some arbritrary mis-alignment
# 1u requires phi= 0.610935 to keep lock
######################## Detection ##########################



#pd1 B2-AC-q $fMI 141 nMPR1 
#pd1 B2-AC-p $fMI 51 nMPR1 
#pd0 B2 nMPR1 

#pd0 B5 nBSAR
#pd1 B5-AC-p $fPDH -13 nBSAR
#pd1 B5-AC-q $fPDH 77 nBSAR

#pd1 B7-AC-q $fMI 41 nMNE2
#pd1 B7-AC-p $fMI 131 nMNE2
#scale 10 B7-AC-p 
#xaxis* MNE phi lin -2m 15m 300
# the zero crossings of the B5 and B7 signals
# differ by 0.0116deg or 34pm


#pd0 B7 nMNE2
#pd0 B8 nMWE2
#pd1 B8-AC-q $fMI 48 nMWE2
#pd1 B8-AC-p $fMI 138 nMWE2
#xaxis* MWE phi lin -1m 1m 1000

pd0 B1p nBSe
pd1 B1p-AC-p $fMI 135 nBSe
pd1 B1p-AC-p1 $fMI 130 nBSe
pd1 B1p-AC-p1 $fMI 140 nBSe
#pd1 B1p-AC-q $fMI 45 nBSe

xaxis BS phi lin -90 90 100

cav NC MNI nMNI2 MNE nMNE1     # compute cavity
cav WC MWI nMWI2 MWE nMWE1     # compute cavity

maxtem 4                       # order of higher order modes (n+m)

yaxis lin abs
#pause
gnuterm no


GNUPLOT
#set cbrange [0:200]
set nosurface
unset hidden3d
set colorbox vertical
set colorbox user origin .9,.1 size .04,.8
#set style line 100 lt 19 lw 0 #gif1
#set style line 100 lt -1 lw 0
#set pm3d at s hidden3d 100 solid
set pm3d map
#set pm3d at s
#-----------------------Colors--------------------------------
# -- traditional pm3d (black-blue-red-yellow)
#set palette rgbformulae 7,5,15
# -- green-red-violet
#set palette rgbformulae 3,11,6
# -- ocean (green-blue-white)  try also all other permutations
#set palette rgbformulae 23,28,3
# -- hot  (black-red-yellow-white)
#set palette rgbformulae 21,22,23
# --  colour printable on gray (black-blue-violet-yellow-white)
#set palette rgbformulae 30,31,32
# -- rainbow (blue-green-yellow-red)
#set palette rgbformulae 33,13,10
# -- AFM hot (black-red-yellow-white)
#set palette rgbformulae 34,35,36
set palette gray
#-------------------------------------------------------------
set title 0,3
set format z '%.1g'
set size ratio .8
unset grid
END
